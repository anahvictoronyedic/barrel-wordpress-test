
<?php

class AjaxEndpoints {

  private const NOUNCE_QUERY_ARG = 'brrl_ajax_nonce';
  private const SCRIPT_HANDLE = 'brrl-ajax-endpoints';
  private const OBJECT_NAME = 'BrrlAjaxEndpointOptions';
  private const ACTION = 'view_post';
  
  public function __construct(){
    
    add_action('wp_ajax_load_post', array( &$this,'ajaxLoadPost') );
    add_action('wp_ajax_nopriv_load_post', array( &$this,'ajaxLoadPost'));

    $script_data_array = array(
      'ajaxurl' => admin_url( 'admin-ajax.php' ),
    );

    $script_data_array[AjaxEndpoints::NOUNCE_QUERY_ARG] = wp_create_nonce( AjaxEndpoints::ACTION );

    // if config should be added directly in head or else use wp_localize_script. 
    // wp_localize_script requires you linking a js file which we dont need for this class
    $inject_directly = true;

    if( $inject_directly ){
      add_action ( 'wp_head', function () use(&$script_data_array){
        ?>
        <script>
          window['<?php echo AjaxEndpoints::OBJECT_NAME?>'] = 
          Object.assign( JSON.parse( '<?= htmlspecialchars( json_encode( $script_data_array ) ,ENT_NOQUOTES ) ?>' ) ,
          window['<?php echo AjaxEndpoints::OBJECT_NAME?>']||{});
        </script>
        <?php
      });
    }
    else{
      wp_register_script(AjaxEndpoints::SCRIPT_HANDLE,null,array(), false, true);
      wp_localize_script( AjaxEndpoints::SCRIPT_HANDLE, AjaxEndpoints::OBJECT_NAME, $script_data_array );
    }
  }

  public function ajaxLoadPost(){

    check_ajax_referer(AjaxEndpoints::ACTION, AjaxEndpoints::NOUNCE_QUERY_ARG);

    $args = array(
      'post_type' => 'post',
      'post_status' => 'publish',
      'p' => $_POST['id'],
    );
 
    $posts = new WP_Query( $args );
 
    $arr_response = array();
    if ($posts->have_posts()) {
 
        while($posts->have_posts()) {
 
            $posts->the_post();
 
            $arr_response = array(
              'title' => get_the_title(),
              'content' => get_the_content(),
            );
        }
        wp_reset_postdata();
    }
 
    echo json_encode($arr_response);
 
    wp_die();
  }
}

new AjaxEndpoints();
