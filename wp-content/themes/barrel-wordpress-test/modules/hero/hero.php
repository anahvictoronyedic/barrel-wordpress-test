<section class="hero">
  <main>

    <?php

    if(true):

      $front_page_id = get_option( 'page_on_front' );

      if(!empty($front_page_id)):

        $description = get_field( 'index__description', $front_page_id );
        $product_image = get_field( 'index__product', $front_page_id ) ;
        $cover_image = get_field( 'index__cover', $front_page_id ) ;
        $headline = get_field( 'index__headline', $front_page_id );

        ?>

        <div class="hero__cover" style="background-image : url( ' <?= esc_attr( $cover_image['url'] ) ?> ') ; 
        background-size:cover;">

          <div class="hero__content hero__row">
            <div class="hero__texts">
              <h1><?= esc_html($headline) ?></h1>
              <h4><?= esc_html($description) ?></h4>
            </div>
            <div class="hero__product-image">
              <img src="<?= esc_attr( $product_image['url'] ) ?>" />
            </div>
          </div>
        </div>

        <?php 

      endif;
    else:
      $image = get_the_post_thumbnail_url( $post->ID, 'large' );
      the_module('image', array(
        'image' => $image
      ));
    endif;
    ?>
  </main>
</section>

<?php 

the_module('featured');
