
// Get the modal
let modal

function openModal (postId) {

  const contentWrapper = modal.querySelector('.post-modal__cwrapper')
  
  const title = modal.querySelector('.post-modal--title')

  // event handler
  function reqListener () {

    const post = JSON.parse(this.response)

    modal.classList.remove('loading')

    title.innerHTML = post.title

    contentWrapper.innerHTML = post.content

  }

  const newXHR = new window.XMLHttpRequest()

  newXHR.addEventListener('load', reqListener)

  newXHR.addEventListener('error', () => {

    modal.classList.remove('loading')

    closeModal()
  })

  newXHR.open('POST', window.BrrlAjaxEndpointOptions['ajaxurl'])

  newXHR.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')

  const formData = 'id=' + postId + '&action=load_post&brrl_ajax_nonce=' + window.BrrlAjaxEndpointOptions['brrl_ajax_nonce']

  title.innerHTML = ''
  contentWrapper.innerHTML = ''

  modal.classList.add('loading')
  modal.style.display = 'block'

  newXHR.send(formData)
}

function closeModal () {
  modal.style.display = 'none'
}

function PostModal () {
  modal = document.querySelector('[data-module="post-modal"]')

  var span = document.getElementsByClassName('post-modal--close')[0]

  span.addEventListener('click', function () {
    closeModal()
  })

  window.addEventListener('click', function (event) {
    if (event.target === modal) {
      closeModal()
    }
  })

  const links = document.querySelectorAll('[data-wp-post-id] a.post--link')

  console.log('PostModal.js found links', links)

  links.forEach((link) => {
    link.addEventListener('click', function (ev) {
      const idHolderElem = link.closest('[data-wp-post-id]')

      if (idHolderElem) {
        let postId = idHolderElem.getAttribute('data-wp-post-id')

        postId = parseInt(postId)

        if (!isNaN(postId)) {
          openModal(postId)

          ev.preventDefault()
        }
      }
    })
  })
}

export default PostModal
