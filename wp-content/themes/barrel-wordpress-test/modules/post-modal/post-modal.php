
<div class="post-modal" data-module="post-modal">
    
  <div class="post-modal__content">

    <div class="post-modal__header">

      <span class="post-modal--close">&times;</span>
      <h2 class="post-modal--title"></h2>

    </div>

    <div class="post-modal__body">

      <div class="post-modal--progress-indc">

        <img width="80" height="80" src="<?= get_template_directory_uri() . '/assets/svg/progress-indicator.svg'; ?>" />
      
      </div>

      <!-- The holding element for the post content -->
      <div class="post-modal__cwrapper">

      </div>

    </div>

    <!--
    <div class="post-modal__footer">
      <h3></h3>
    </div> -->

  </div>

</div>

<?php
