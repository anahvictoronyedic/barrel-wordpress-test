
<?php 

// Get the latest post
$query = new WP_Query( array( 'posts_per_page' => 1 ) );

if(!$query->have_posts()) return;

$query->the_post();

$post__image = featured_image_or_fallback($query);
$post__image_position = image_custom_position($query);
$post__icon = get_format_icon($query);
$post__cta = get_cta($query); 

?>

<section class="featured" data-wp-post-id="<?= get_the_ID() ?>">
  
  <div class="featured__wrap featured__row">

    <a href="<?php the_permalink(); ?>" class="post--link">
      <div class="featured__image" style="background-image:url(' <?= esc_attr( $post__image ) ?> '); background-position:<?= $post__image_position; ?>">
      </div>
    </a>

    <div class="featured__post post__meta">
      <div class="post__meta--icon">
        <?php echo $post__icon; ?>
      </div>
      <a href="<?php echo get_month_link(get_the_date('Y'), get_the_date('m')); ?>" class="post__meta--date">
        <button><?php echo get_the_date('F d'); ?></button>
      </a>
      <a href="<?php the_permalink(); ?>" class="post__meta--title post--link">
        <h4><?php echo get_the_title(); ?></h4>
      </a>
      <a href="<?php the_permalink(); ?>" class="post__meta--title post--link">
        <p><?php echo get_the_excerpt(); ?></p>
      </a>
      <a href="<?php the_permalink(); ?>" class="post__meta--cta post--link">
        <?php echo $post__cta; ?>
      </a>
    </div>
  </div>

</section>

<?php

wp_reset_postdata();
