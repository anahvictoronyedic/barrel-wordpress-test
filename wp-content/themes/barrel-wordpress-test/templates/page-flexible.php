<?php
  /*
  Template Name: Flexible
  Template Post Type: post, page
  */
  get_header();

  the_module('hero');

  the_module('layout');

  the_module('post-modal');

  get_footer();
?>
